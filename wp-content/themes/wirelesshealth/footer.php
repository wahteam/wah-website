    </div><!-- #main -->
 
    <footer class="container-fluid no-padding">
    	<div class="container footer-widget">
    		<div class="row">
	    		<div class="col-md-3">
	    			            <a href="<?php echo site_url(); ?>">
                <img src="<?php echo get_bloginfo('template_directory');?>/img/wah-logo.png" class="wah-logo" />
            </a>
			    	<div class="footer-widget-title">Wireless Access for Health</div>
		            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis nisi risus, et volutpat enim consequat sit amet. </p>
	    		</div>
	    		<div class="col-md-3">
	    			<div class="footer-widget-title">Site Map</div>
	    			<div class="footer-breakline"></div>
	    			<div class="footer-menu">
	    			<ul>
	    				<li>About Us</li>
	    				<li>Services</li>
	    				<li>Contact Us</li>
	    			</ul>
	    			</div>
	    		</div>
	    		<div class="col-md-3">
	    			<div class="footer-widget-title">WAH Office</div>
	    			<div class="footer-breakline"></div>
	    			<div>
	    				<p>2/F Room 201 TPHO Dormitory, Hospital Drive, 
						San Vicente, Tarlac City 2300, Philippines</p>
						<p>(045) 982-1246</p>
	    			</div>
	    		</div>
	    		<div class="col-md-3">
	    			<div class="footer-widget-title">Newsletter</div>
	    			<div class="footer-breakline"></div>
	    			<div>
	    				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
	    			</div>
	    		</div>	    		
	    	</div>
	    	<div>2016. Wireless Access for Health. All Rights Reserved.</div>
    	</div>
    </footer>
<?php wp_footer(); ?>
</div>
</body>
</html>