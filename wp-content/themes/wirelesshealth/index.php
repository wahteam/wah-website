<?php get_header(); ?>

<section>
	<!-- CAROUSEL -->
	<?php get_template_part('page-carousel');  ?>
</section>
<section class="section">
	<!-- HOME WIDGET -->
	<?php get_template_part('page-homewidget');  ?>
</section>
 <section class="section about-page">
 	<div class="container">
		<div class="dark-line"><h2><span>About Us</span></h2></div>
		<?php
	    $aboutus = get_page_by_title('About Us');
	    ?>
	    <div class="row">
		    <div class="col-md-6 about-description">
		    	<p><?php echo $aboutus->post_content; ?></p>
		    </div>
		    <div class="col-md-6">
			<?php
			    $cat_id = get_cat_ID('About Us');
			    $args = array(
			        'cat' => $cat_id,
			        'post_type' => 'home-widget',
			        'order' => 'ASC'
			    );
			    $widgets = new WP_Query($args);
			    if ($widgets->have_posts()):
			    while ($widgets->have_posts()): $widgets->the_post();
			        $feat_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
			        ?>
			        <div class="about-sub-content">    
			           <div class="about-icon">                 
			            <img class="img-responsive" src="<?php echo $feat_image[0]; ?>"/>   
			            </div>         
			            <div class="about-widget-title"><?php the_title(); ?></div>
			            <div class="widget-description"><?php the_content(); ?></div>
			        </div>
			        <?php
			    endwhile;
				endif;
			?>
			</div>
		</div>
	</div>
</section>
 <section class="section">
	<div class="container">
		<div class="page-title">Our Partner</div>
		<div class="partners">
			<?php
			    $args = array(
			        'post_type' => 'partner',
			        'order' => 'ASC'
			    );
			    $widgets = new WP_Query($args);
			    if ($widgets->have_posts()):
			    while ($widgets->have_posts()): $widgets->the_post();
			        $feat_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
			        ?>
			        <div class="">                   
			            <img class="img-responsive" src="<?php echo $feat_image[0]; ?>"/>  
			        </div>
			        <?php
			    endwhile;
				endif;
			?>
		</div>
	</div>
</section>
 <section>
	<!-- RECENT EVENTS -->
	<div>REcent Events</div>
</section>
 <section>
	<!-- TESTIMONIAL -->
</section>
<?php get_footer(); ?>