<div class="container">
<div class="page-title">Features</div>
<?php
    $cat_id = get_cat_ID('Features');
    $args = array(
        'cat' => $cat_id,
        'post_type' => 'home-widget',
        'order' => 'ASC'
    );
    $widgets = new WP_Query($args);
    if ($widgets->have_posts()):
    while ($widgets->have_posts()): $widgets->the_post();
        $feat_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
        ?>
        <div class="col-md-4 col-sm-4 col-xs-4 widget-content">    
           <div class="widget-icon">                 
            <img class="img-responsive" src="<?php echo $feat_image[0]; ?>"/>   
            </div>         
            <div class="widget-title"><?php the_title(); ?></div>
            <div class="widget-breakline"></div>
            <div class="widget-description"><?php the_excerpt(); ?></div>
        </div>
        <?php
    endwhile;
	endif;
?>
</div>