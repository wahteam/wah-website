<div class="container-fluid no-padding">
<ul class="bxslider">
	<?php 
		$args = array('post_type' => 'carousel');
        $slides = new WP_Query($args);
        if ($slides->have_posts()):
                while ($slides->have_posts()): $slides->the_post();
	?>
	  <li>
	  	<?php 
			$feat_image = wp_get_attachment_url(get_post_thumbnail_id());
		?>
	  	<img src="<?php echo $feat_image; ?>" />
	    <div class="table-container">
		    <div class="center-container">
		        <div class="img-title"><?php the_title(); ?></div>
		        <div class="break-line"></div>
		        <div class="carousel-content">
		        	<?php 
		        		the_content();
		        	?>
		        </div>
		    </div>
		</div>
	  </li>
  <?php 
  endwhile;
  endif;
  ?>
</ul>
</div>