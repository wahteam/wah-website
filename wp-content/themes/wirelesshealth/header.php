<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php wp_title(); ?>
    </title>

    <?php wp_head(); ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

</head>

<body>
    <div class="main-content">
        <header class="container site-header">
            <a href="<?php echo site_url(); ?>">
                <img src="<?php echo get_bloginfo('template_directory');?>/img/logo.png" class="wah-logo" />
            </a>
            <?php 
                $menu = wp_nav_menu( array(
                    'menu' => 'Project Nav'
                ) );

                echo $menu;
            ?>
        </header>
        <a href="#main-content" class="back-top"></a>
        <div id="main-content">